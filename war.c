#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CARDS 13

//Struct for cards
typedef struct Card{
    char face[5];
    int value;
} Card;

//Struct for players
typedef struct Player{
	int name;
	int score;
}Player;

//Function declarations
void initilizePlayerScores(Player player[], int numberOfPlayers);
void createPlayerDecks(Player player[], Card playerDeck[][MAX_CARDS], Card cardsHardcoded[], int numberOfPlayers);
int findGameWinner(Player player[], int numberOfPlayers);
void showPlayerCards(Player player,Card playerDeck[][MAX_CARDS]);
int playerPickCard(Player player, Card playerDeck[][MAX_CARDS]);
int roundScore(int roundCardValueArr[], int numberOfPlayers);
int roundWinner(Player player[], int roundCardValueArr[], int numberOfPlayers);
int chekcIfAllCardsTie(int tieCards[], int numberOfPlayers);

int main()
{
    //Variables declariation and initialization
    int numberOfPlayers;
    int roundNumber = 1;
    int gameWinner;
    int roundRollOverScore = 0;
    int rollOverTrue = 0;
    int thisRoundScore = 0;
    char keyContinue;

    //Game start msgs and user input
    printf("Welcome to the game of War!\n");
    printf("Enter number of players: ");
    scanf("%d", &numberOfPlayers);
    printf("Let the game of War begin!");

    system("@cls||clear");

    //Create player and card decks struct arrays
    Player player[numberOfPlayers];
    Card playerDeck[numberOfPlayers][MAX_CARDS];

    //Create round players cards values array
    int roundCardValueArr[numberOfPlayers];

    //Create and hardcode cards face and value struct array
    Card cardsHardcoded[MAX_CARDS] = {
        {"2", 2}, {"3", 3}, {"4", 4}, {"5", 5}, {"6", 6}, {"7", 7}, {"8", 8}, {"9", 9},
                        {"10", 10}, {"J", 11}, {"Q", 12}, {"K", 13}, {"A", 14}
    };

    //Create individual players decks from hardcoded deck
    createPlayerDecks(player, playerDeck, cardsHardcoded, numberOfPlayers);

    //Initialize player scores to zero
    initilizePlayerScores(player, numberOfPlayers);

    //While loop for rounds
    ROUND_LOOP:while(roundNumber != 14){
        printf("Start of Round %d.\n", roundNumber);
        //For loop for players card selection
        for(int i = 0; i < numberOfPlayers; ++i){
            printf("\nPlayer %d, Round %d.\n", i+1, roundNumber);
            showPlayerCards(player[i], playerDeck);
            //Add card values to round card value array
            roundCardValueArr[i] = playerPickCard(player[i], playerDeck);

            printf("\n\nPress ENTER for next player turn.\n");
            scanf("%c",&keyContinue);
            scanf("%c",&keyContinue);

            system("@cls||clear");
        }
        //Get round score
        thisRoundScore = roundScore(roundCardValueArr,numberOfPlayers);
        //Check if round was rolled over
        rollOverTrue = roundWinner(player,roundCardValueArr,numberOfPlayers), roundScore(roundCardValueArr,numberOfPlayers);

        //Round roll over if statement
        if(rollOverTrue == 11){
            roundRollOverScore += roundScore(roundCardValueArr,numberOfPlayers);
            printf("All Players tie. Points carried to next round: %d", roundRollOverScore);
            printf("\nVery important round!\n\n");
            roundNumber++;
            //Go to start of loop and begin next round
            goto ROUND_LOOP;
        //Round wasnt rolled over
        }else{
            printf("Total score of this round is %d\n", thisRoundScore);
            printf("Winner of this round is Player %d with score of %d. Congrats!\n",
                   roundWinner(player,roundCardValueArr,numberOfPlayers) + 1, thisRoundScore + roundRollOverScore);
            //Count total player score
            player[roundWinner(player,roundCardValueArr,numberOfPlayers)].score += roundScore(roundCardValueArr, numberOfPlayers) + roundRollOverScore;
            roundRollOverScore = 0;
        }

        roundNumber++;
    }

    system("@cls||clear");
    //Get game winner
    gameWinner = findGameWinner(player, numberOfPlayers);

    printf("The War is finished!\n");
    printf("\n\nWinner is Player %d, with total score of %d.\n\n\n", gameWinner + 1, player[gameWinner].score);
}
//Function to initialize player scores to ze
void initilizePlayerScores(Player player[], int numberOfPlayers){
    for(int i = 0; i < numberOfPlayers; ++i){
        player[i].score = 0;
    }
}

//Function to create player decks from hardcoded cards struct, assign player names
void createPlayerDecks(Player player[], Card playerDeck[][MAX_CARDS], Card cardsHardcoded[], int numberOfPlayers){
    for(int i = 0; i < numberOfPlayers; ++i){
        player[i].name = i;
        for(int j = 0; j < MAX_CARDS; ++j){
            playerDeck[i][j] = cardsHardcoded[j];
        }
    }
}

//Find game winner using for loop and player.scores from struct
int findGameWinner(Player player[], int numberOfPlayers){
    int largest = 0;
    int winner = 0;

    for(int i = 0; i < numberOfPlayers; ++i){
        if(player[i].score > largest){
            largest = player[i].score;
            winner = i;
        }
    }
    return winner;
}

//Show individual player cards used for displaying available cards and indexes
void showPlayerCards(Player player,Card playerDeck[][MAX_CARDS]){
    char usedCard[] = "_";

    //Display player deck cards faces
    printf("\n");
    printf("Card face: ");
    for(int i = 0; i < MAX_CARDS; ++i){
        printf("| %2s ", playerDeck[player.name][i].face);
    }

    printf("\n");

    //Display card indexes for player choice, and change to usedCard if already used
    printf("Choice:    ");
    for(int i = 0; i < MAX_CARDS; ++i){
        if(strcmp(playerDeck[player.name][i].face, usedCard) != 0){
            printf("| %2d ", i + 1);
        }else{
            printf("| %2s ", usedCard);
        }
    }
    printf("\n");
}

//Individual players pick card
int playerPickCard(Player player,Card playerDeck[][MAX_CARDS]){
    int cardIndex;
    char unavailable[] = "_";

    printf("\nPlayer %d turn.Your card: ", player.name + 1);
    scanf("%d", &cardIndex);

    //printf("You chose to play %2s", playerDeck[0][cardIndex - 1].face);

    //Change face value tu unavailabe after choice is made
    strcpy(playerDeck[player.name][cardIndex - 1].face, unavailable);

    return playerDeck[player.name][cardIndex -1].value;
}

//Count round score using passed array of all players round card values
int roundScore(int roundCardValueArr[], int numberOfPlayers){
    int roundScore = 0;

    for(int i = 0; i < numberOfPlayers; ++i){
        roundScore += roundCardValueArr[i];
    }
    return roundScore;
}

//Check for tie's and find round winner
int roundWinner(Player player[], int roundCardValueArr[], int numberOfPlayers){

    int tieCards[numberOfPlayers];
    int tieArr[numberOfPlayers];
    int allTie = 11;

    //Initialize tieCards array values to 0 for correct functioning of tie checking
    for(int i = 0; i < numberOfPlayers; ++i){
        tieCards[i] = 0;
    }
    //For loop to check if any cards tie using rounds card value array
    for(int i = 0; i < numberOfPlayers; ++i){
        for(int j = i + 1; j < numberOfPlayers; ++j){
            //If card values match, corresponding tieCards array values set to 1, meaning a tie
            if(roundCardValueArr[i] == roundCardValueArr[j]){
                tieCards[i] = 1;
                tieCards[j] = 1;
            }
        }
    }

    //Check if all players tie
    if(chekcIfAllCardsTie(tieCards,numberOfPlayers) == 1){
        //If all cards tie exit function and return allTie
        return allTie;
    }

    //Using for loop to initialize values of tieArr only to non-tie round values
    for(int i = 0; i < numberOfPlayers; ++i){
        if(tieCards[i] == 1){
            tieArr[i] = 0;
        }else{
            //Set tie arr to non-tie values
            tieArr[i] = roundCardValueArr[i];
        }
    }

    //Find winning player index using tieArr of only non-tie values
    int winner = 0;
    int largest = 0;
    for(int i = 0; i < numberOfPlayers; ++i){
        if(tieArr[i] > largest){
            largest = tieArr[i];
            winner = i;
        }
    }
    return winner;
}
//Function to check if all cards were tied
int chekcIfAllCardsTie(int tieCards[], int numberOfPlayers){
    int cardsTie = 0;

    for(int i = 0; i < numberOfPlayers; ++i){
        cardsTie += tieCards[i];
    }

    if(cardsTie == numberOfPlayers){
        return 1;
    }
    return 0;
}














